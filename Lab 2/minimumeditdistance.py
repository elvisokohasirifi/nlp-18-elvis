import sys

def minEdit(source, target):
	n = len(source)
	m = len(target)

	D = [[0]]
	for i in range(1, n + 1):
		D.append([D[i-1][0] + 1])
	for i in range(1, m + 1):
		D[0].append(D[0][i-1] + 1)

	sub = 0
	for i in range(1, n + 1):
		for j in range(1, m + 1):
			if(source[i-1] == target[j-1]):
				sub = 0
			else:
				sub = 2
			D[i].insert(j, min(D[i-1][j] + 1, D[i-1][j-1] + sub, D[i][j-1] + 1))

	print("Minimum edit distance between", source, "and", target, "is", D[n][m])

if __name__ == "__main__":
    source = sys.argv[1]
    target = sys.argv[2]
    minEdit(source, target)
