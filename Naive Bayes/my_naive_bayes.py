from math import *
def trainNaiveBayes(D, C, Vocabs, Cwords):
	logpriors = {}
	loglikelihoods = {}
	for c in C.keys():
		loglikelihoods[c] = {}
		Ndoc = len(D)
		Nc = len(C[c])
		logprior = log(Nc/Ndoc)
		logpriors[c] = logprior
		V = Vocabs
		a = Cwords[c]
		bigdoc = a
		den = denominator(V, Cwords[c])
		for w in V:
			countwc = bigdoc.count(w)
			loglikelihoodwc = log((countwc + 1)/den)
			loglikelihoods[c][w] = loglikelihoodwc

	return logpriors, loglikelihoods, V

def denominator(V, C):
	total = 0
	for i in V:
		total += C.count(i)
	return total + len(V)

def testNaiveBayes(testdoc, logprior, loglikelihood, C, V):
	Sumc = {}
	for c in C:
		sumc = logprior[c]
		for i in testdoc.split(" "):
			if i in V:
				sumc = sumc + loglikelihood[c][i]
		Sumc[c] = sumc
	bigClass = -99999
	bigC = None
	for val in Sumc.keys():
		if Sumc[val] > bigClass:
			bigClass = Sumc[val]
			bigC = val
	return bigC



def importFile(filename):
	file = open(filename).readlines()
	C = {}
	Cwords = {}
	D = []
	for line in file:
		broken = line.split("\t")
		if broken[1].strip("\n") in C.keys():
			C[broken[1].strip("\n")].append(broken[0].strip("\t1\n").strip("."))
			a = broken[0].strip("\t1\n").strip(".").strip("!").split(" ")
			Cwords[broken[1].strip("\n")] = Cwords[broken[1].strip("\n")] + a
		else:
			C[broken[1].strip("\n")] = [broken[0].strip("\t1\n").strip(".")]
			a = broken[0].strip("\t1\n").strip(".").strip("!").split(" ")
			Cwords[broken[1].strip("\n")] = a
		D.append(broken[0].strip("\t1\n").strip("."))
	return C, D, Cwords

def getVocabs(D):
	dicts = {}
	for i in D:
		broken = i.strip("\n").split(" ")
		for j in broken:
			dicts[j] = 0
	return dicts

def main(trainfile, testfile):
	C, D, Cwords = importFile(trainfile)
	Vocabs = getVocabs(D)
	logpriors, loglikelihoods, V = trainNaiveBayes(D, C, Vocabs, Cwords)
	test = open(testfile).readlines()
	count = 0
	outputf = open("result.txt", "w")
	for i in test:
		splited = i.split("\t")
		ans = testNaiveBayes(splited[0], logpriors, loglikelihoods, C, V)
		print(splited[0] + "\t" + ans, file=outputf)
		if ans == splited[1].strip("\n"):
			count += 1
	print(count, "/", len(test), "=", round(count/len(test) * 100, 2), "%")
	outputf.close()
		
main("yelp_labelled.txt", "test.txt")
#print(importFile("imdb_labelled.txt")[2])

